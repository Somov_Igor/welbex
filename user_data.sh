#!/bin/bash
sudo apt update
sudo apt-get -y install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
mkdir ~/HelloWorld
cat <<EOF > ~/HelloWorld/index.html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Docker Nginx</title>
</head>
<body>
  <center><h3>Hello World, This is Nginx container</h3></center>
</body>
</html>
EOF
sudo docker run --name nginx01 -p 80:80 -p 443:443 -d -v ~/HelloWorld:/usr/share/nginx/html nginx
